.PHONY: help

.DEFAULT_GOAL := help

help: ## Display help
	@echo "  _____      _______ _____  _____ "
	@echo " / ____|  /\|__   __|_   _|/ ____|"
	@echo "| (____  /  \  | |    | | | (___ "
	@echo " \___ \ / /\ \ | |    | |  \___ \ "
	@echo " ____) / ____ \| |   _| |_ ____) |"
	@echo "|_____/_/    \_\_|  |_____|_____/"
	@echo " "
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

build: ## Build latest modifications to Satis
	#sudo chown adminapp:www-data -R .
	#sudo chmod -R 776 .
	docker run --rm --init -it \
		  --user $(id -u):$(id -g) \
		    --volume /home/cpro/satis:/srv/satis \
		      --volume "/home/cpro/.composer:/composer" \
		        --volume "/home/cpro/.ssh:/root/.ssh" \
			  composer/satis:1.x build /srv/satis/satis.json /srv/satis/web
	#bin/satis build satis.json web/

add: ## Add a new repository to Statis through parameter "source" ex: make add source="git@mypackage.git"
	bin/satis add $(source)

commit: ## Commit changes
	@git add .
	@git commit -m "Build du $$(date '+%d/%m/%Y %H:%M')"

publish: build commit ## Build, commit and deploy changes
	@git push
